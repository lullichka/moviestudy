package com.lulz.games.moviestudyapp.dagger.app;


import com.lulz.games.moviestudyapp.BuildConfig;
import com.lulz.games.moviestudyapp.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {
        ServiceModule.class
})
public class NetworkModule {
    @Singleton
    @Provides
    static HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Singleton
    @Provides
    static OkHttpClient provideHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(Constants.TIME_OUT, TimeUnit.SECONDS);
        httpClient.readTimeout(Constants.TIME_OUT, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(loggingInterceptor);
        }
        return httpClient.build();
    }

    @Singleton
    @Provides
    static Retrofit provideRetrofit(OkHttpClient httpClient) {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
    }
}
