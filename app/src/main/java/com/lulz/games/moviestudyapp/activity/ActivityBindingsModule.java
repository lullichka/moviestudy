package com.lulz.games.moviestudyapp.activity;

import android.app.Activity;

import com.lulz.games.moviestudyapp.presentation.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        MainActivityComponent.class,
})
public abstract class ActivityBindingsModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity> mainActivityComponentBuilder(MainActivityComponent.Builder builder);

}
