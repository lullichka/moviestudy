package com.lulz.games.moviestudyapp.data;

import com.lulz.games.moviestudyapp.model.Movie;
import com.lulz.games.moviestudyapp.model.MovieResult;

import io.reactivex.Single;

/**
 * Created by Julia Alekseeva on 10/10/18.
 */

public interface DeviceStorage {
    Single<MovieResult> getMoviesInfo(int page);


    Single<Movie> getMovie(int id);
}
