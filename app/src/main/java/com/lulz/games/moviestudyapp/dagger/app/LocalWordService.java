package com.lulz.games.moviestudyapp.dagger.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import dagger.android.DaggerService;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

public class LocalWordService extends DaggerService {


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new LocalWordServiceBinder<>(this);
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

}
