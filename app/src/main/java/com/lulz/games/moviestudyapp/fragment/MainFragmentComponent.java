package com.lulz.games.moviestudyapp.fragment;

import com.lulz.games.moviestudyapp.MainFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Julia Alekseeva on 10/12/18.
 */

@FragmentScope
@Subcomponent
public interface MainFragmentComponent extends AndroidInjector<MainFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainFragment> {

    }

}