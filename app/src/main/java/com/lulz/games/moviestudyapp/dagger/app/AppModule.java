package com.lulz.games.moviestudyapp.dagger.app;

import android.content.Context;

import com.lulz.games.moviestudyapp.R;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.lulz.games.moviestudyapp.Constants.API_KEY;

@Module
public class AppModule {

    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    @Named(API_KEY)
    static String provideApiKey(Context context) {
        return context.getString(R.string.api_key);
    }

}
