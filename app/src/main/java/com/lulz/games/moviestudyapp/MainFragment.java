package com.lulz.games.moviestudyapp;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lulz.games.moviestudyapp.common.BaseFragment;
import com.lulz.games.moviestudyapp.common.BasePresenter;
import com.lulz.games.moviestudyapp.databinding.FragmentMainBinding;
import com.lulz.games.moviestudyapp.fragment.FragmentScope;
import com.lulz.games.moviestudyapp.model.MovieResult;
import com.lulz.games.moviestudyapp.presentation.DetailFragment;
import com.lulz.games.moviestudyapp.presentation.MainContract;
import com.lulz.games.moviestudyapp.presentation.MainPresenter;
import com.lulz.games.moviestudyapp.presentation.MovieAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

@FragmentScope
public class MainFragment extends BaseFragment<FragmentMainBinding> implements MainContract.View {

    @Inject
    MainPresenter mainPresenter;
    private MovieAdapter mMoviesAdapter;
    private GridLayoutManager layoutManager;
    private int lastVisibleItem, totalItemCount;
    private int pageNumber = 1;
    private final int VISIBLE_THRESHOLD = 1;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public BasePresenter getPresenter() {
        return mainPresenter;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initView() {
        mainPresenter.attachView(this);
        getBinding().setEventListener(mainPresenter);
        retrieveMovies();
        initList();
    }

    private void initList() {
        ArrayList mMoviesList = new ArrayList<>();
        mMoviesAdapter = new MovieAdapter(mContext, mMoviesList, mainPresenter);
        layoutManager = new GridLayoutManager(mContext, 2);
        getBinding().rvMovies.setLayoutManager(layoutManager);
        getBinding().rvMovies.setAdapter(mMoviesAdapter);
        setUpLoadMoreListener();

    }

    private void setUpLoadMoreListener() {
        getBinding().rvMovies.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager
                        .findLastVisibleItemPosition();
                if (!mainPresenter.getLoading() && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    pageNumber++;
                    retrieveMovies();
                    mainPresenter.setLoading(true);
                }
            }
        });
    }

    private void retrieveMovies() {
        mainPresenter.getMoviesList(pageNumber);
    }

    @Override
    public void updateInfo(MovieResult movies) {
        mMoviesAdapter.replaceData(movies.getResults());
    }

    @Override
    public void openMovieDetailFragment(int movieId) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, DetailFragment.newInstance(movieId), "DetailFragment")
                .addToBackStack("DetailFragment")
                .commit();    }

}
