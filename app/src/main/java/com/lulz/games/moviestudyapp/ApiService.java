package com.lulz.games.moviestudyapp;

import com.lulz.games.moviestudyapp.model.Movie;
import com.lulz.games.moviestudyapp.model.MovieResult;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public interface ApiService {

    @GET("movie/{kind}")
    Single<MovieResult> getMovies(
            @Path("kind") String kind,
            @Query("api_key") String key,
            @Query("page") int page,
            @Query("region") String region);


    @GET("movie/{movie_id}")
    Single<Movie> getMovie(
            @Path("movie_id") int movieId,
            @Query("api_key") String key,
            @Query("append_to_response") String cast);

}
