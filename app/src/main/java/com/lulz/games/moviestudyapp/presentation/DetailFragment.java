package com.lulz.games.moviestudyapp.presentation;

import android.os.Bundle;

import com.lulz.games.moviestudyapp.R;
import com.lulz.games.moviestudyapp.common.BaseFragment;
import com.lulz.games.moviestudyapp.common.BasePresenter;
import com.lulz.games.moviestudyapp.databinding.FragmentDetailBinding;
import com.lulz.games.moviestudyapp.fragment.FragmentScope;
import com.lulz.games.moviestudyapp.model.Movie;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

@FragmentScope
public class DetailFragment extends BaseFragment<FragmentDetailBinding> implements DetailContract.View {

    private static final String MOVIE_ID = "movie_id";
    private static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w185/";


    @Inject
    DetailPresenter detailPresenter;


    public static DetailFragment newInstance(int id) {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        args.putInt(MOVIE_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void updateMovie(Movie movie) {
        getBinding().setMovie(movie);
        Picasso.with(mContext)
                .load(BASE_IMAGE_URL + movie.getBackdrop_path())
                .fit()
                .centerCrop()
                .into(getBinding().ivTrailer);
    }

    @Override
    public BasePresenter getPresenter() {
        return detailPresenter;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_detail;
    }

    @Override
    protected void initView() {
        int movieId = getArguments().getInt(MOVIE_ID);
        detailPresenter.attachView(this);
        getBinding().setEventListener(detailPresenter);
        detailPresenter.getMovie(movieId);
    }
}
