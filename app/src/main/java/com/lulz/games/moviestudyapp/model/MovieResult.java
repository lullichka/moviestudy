package com.lulz.games.moviestudyapp.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public class MovieResult {

    @SerializedName("results")
    @Expose
    private List<Movie> results = new ArrayList<>();

    public List<Movie> getResults() {
        return results;
    }

}
