package com.lulz.games.moviestudyapp;

import android.app.Activity;
import android.app.Application;
import android.app.Service;

import com.lulz.games.moviestudyapp.dagger.app.AppModule;
import com.lulz.games.moviestudyapp.dagger.app.DaggerAppComponent;
import com.lulz.games.moviestudyapp.dagger.app.NetworkModule;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;

public class App extends Application implements HasActivityInjector, HasServiceInjector {
    @Inject
    DispatchingAndroidInjector<Activity> mAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Service> mAndroidServiceInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .create(this)
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mAndroidInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return mAndroidServiceInjector;
    }
}
