package com.lulz.games.moviestudyapp.presentation;

import android.util.Log;

import com.lulz.games.moviestudyapp.common.BasePresenter;
import com.lulz.games.moviestudyapp.data.DeviceStorageRemote;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public class MainPresenter implements BasePresenter<MainContract.View>, MainContract.Presenter {
    private static final String LOG_TAG = MainPresenter.class.getSimpleName();
    private DeviceStorageRemote mDeviceStorageRemote;
    private final CompositeDisposable mCompositeDisposable;
    private MainContract.View mView;
    private boolean loading = false;


    @Inject
    MainPresenter(DeviceStorageRemote deviceStorageRemote) {
        mDeviceStorageRemote = deviceStorageRemote;
        mCompositeDisposable = new CompositeDisposable();
    }


    @Override
    public void getMoviesList(int page) {
        mCompositeDisposable.add(mDeviceStorageRemote.getMoviesInfo(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movies -> {
                            mView.updateInfo(movies);
                            loading = false;
                        },
                        e -> Log.e(LOG_TAG, e.getMessage())));
    }

    @Override
    public void getMovie(int id) {
        mView.openMovieDetailFragment(id);
    }

    @Override
    public void setLoading(boolean loading) {
        this.loading = loading;

    }

    @Override
    public boolean getLoading() {
        return loading;
    }

    @Override
    public void attachView(MainContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mCompositeDisposable.dispose();
        mView = null;
    }
}
