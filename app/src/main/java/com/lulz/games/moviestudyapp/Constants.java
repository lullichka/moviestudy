package com.lulz.games.moviestudyapp;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public class Constants {
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "api_key";

    public static final long TIME_OUT = 60;

    public static final String STORAGE_REMOTE = "remote";
}
