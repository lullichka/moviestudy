package com.lulz.games.moviestudyapp.dagger.app;


import com.lulz.games.moviestudyapp.App;
import com.lulz.games.moviestudyapp.activity.ActivityBindingsModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        ActivityBindingsModule.class,
        ServiceBindingModule.class,
        AndroidSupportInjectionModule.class
})
public interface AppComponent extends AndroidInjector<App> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<App> {
        public abstract Builder appModule(AppModule module);

        public abstract Builder networkModule(NetworkModule module);

    }
}
