package com.lulz.games.moviestudyapp.dagger.app;

import com.lulz.games.moviestudyapp.activity.ActivityScope;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

@Module
public abstract class ServiceBindingModule {
    @ActivityScope
    @ContributesAndroidInjector()
    abstract LocalWordService contributeLocalWordServiceInjector();
}
