package com.lulz.games.moviestudyapp.presentation;

import com.lulz.games.moviestudyapp.model.Movie;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

public interface DetailContract {
    interface View {

        void updateMovie(Movie movie);
    }

    interface Presenter {

        void getMovie(int id);

    }

}
