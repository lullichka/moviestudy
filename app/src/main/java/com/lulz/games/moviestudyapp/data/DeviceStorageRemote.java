package com.lulz.games.moviestudyapp.data;

import com.lulz.games.moviestudyapp.ApiService;
import com.lulz.games.moviestudyapp.model.Movie;
import com.lulz.games.moviestudyapp.model.MovieResult;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Single;

import static com.lulz.games.moviestudyapp.Constants.API_KEY;

/**
 * Created by Julia Alekseeva on 10/10/18.
 */

public class DeviceStorageRemote implements DeviceStorage {
    private final ApiService mApiService;
    private String mApiKey;

    @Inject
    DeviceStorageRemote(@Named(API_KEY) String apiKey, ApiService apiService) {
        mApiKey = apiKey;
        mApiService = apiService;
    }

    @Override
    public Single<MovieResult> getMoviesInfo(int page) {
        return mApiService.getMovies("popular", mApiKey, page, "US");
    }

    @Override
    public Single<Movie> getMovie(int id) {
        return mApiService.getMovie(id, mApiKey, "credits, videos");
    }
}
