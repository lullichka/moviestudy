package com.lulz.games.moviestudyapp.common;

public interface BasePresenter<V> {
    void attachView(V view);

    void detachView();
}