package com.lulz.games.moviestudyapp.presentation;


import com.lulz.games.moviestudyapp.model.MovieResult;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public interface MainContract {
    interface View {
        void updateInfo(MovieResult movies);

        void openMovieDetailFragment(int movieId);
    }

    interface Presenter {
        void getMoviesList(int page);

        void getMovie(int id);

        void setLoading(boolean loading);

        boolean getLoading();
    }
}
