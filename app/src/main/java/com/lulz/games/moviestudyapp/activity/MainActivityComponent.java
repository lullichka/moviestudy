package com.lulz.games.moviestudyapp.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.lulz.games.moviestudyapp.MainFragment;
import com.lulz.games.moviestudyapp.fragment.DetailFragmentComponent;
import com.lulz.games.moviestudyapp.fragment.MainFragmentComponent;
import com.lulz.games.moviestudyapp.presentation.DetailFragment;
import com.lulz.games.moviestudyapp.presentation.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */


@ActivityScope
@Subcomponent(modules = {
        MainActivityComponent.MainActivityModule.class,
        MainActivityComponent.FragmentBindingsModule.class
})
public interface MainActivityComponent extends AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {
    }

    @Module
    class MainActivityModule {
        @Provides
        AppCompatActivity provideActivity(MainActivity activity) {
            return activity;
        }

    }

    @Module(subcomponents = {
            MainFragmentComponent.class,
            DetailFragmentComponent.class
    })
    abstract class FragmentBindingsModule {

        @Binds
        @IntoMap
        @FragmentKey(MainFragment.class)
        public abstract Factory<? extends Fragment> mainFragmentComponentBuilder(MainFragmentComponent.Builder builder);


        @Binds
        @IntoMap
        @FragmentKey(DetailFragment.class)
        public abstract Factory<? extends Fragment> detailFragmentComponentBuilder(DetailFragmentComponent.Builder builder);
    }

}
