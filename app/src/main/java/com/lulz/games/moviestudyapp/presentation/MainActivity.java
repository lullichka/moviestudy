package com.lulz.games.moviestudyapp.presentation;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import com.lulz.games.moviestudyapp.MainFragment;
import com.lulz.games.moviestudyapp.R;
import com.lulz.games.moviestudyapp.common.BaseActivity;
import com.lulz.games.moviestudyapp.dagger.app.LocalWordService;
import com.lulz.games.moviestudyapp.dagger.app.LocalWordServiceBinder;

public class MainActivity extends BaseActivity implements ServiceConnection {

    private ViewDataBinding mBinding;
    private LocalWordService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame, MainFragment.newInstance(), null)
                .commit();
        bindService(new Intent(this, LocalWordService.class), this, BIND_AUTO_CREATE);

    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service) {
        mService = ((LocalWordService) ((LocalWordServiceBinder<Service>) service).getService());
        Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mService = null;
    }


}
