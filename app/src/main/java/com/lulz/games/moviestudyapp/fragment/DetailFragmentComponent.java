package com.lulz.games.moviestudyapp.fragment;

import com.lulz.games.moviestudyapp.presentation.DetailFragment;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

@FragmentScope
@Subcomponent
public interface DetailFragmentComponent extends AndroidInjector<DetailFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DetailFragment> {

    }

}