package com.lulz.games.moviestudyapp.presentation;

import android.content.Context;

import com.lulz.games.moviestudyapp.R;
import com.lulz.games.moviestudyapp.common.BindingAdapter;
import com.lulz.games.moviestudyapp.common.BindingViewHolder;
import com.lulz.games.moviestudyapp.databinding.RecyclerviewItemBinding;
import com.lulz.games.moviestudyapp.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Julia Alekseeva on 10/9/18.
 */

public class MovieAdapter extends BindingAdapter<RecyclerviewItemBinding> {
    private static final String BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w185/";
    private List<Movie> movieList;
    private Context mContext;
    private MainContract.Presenter mEventListener;


    public MovieAdapter(Context context, List<Movie> movies, MainContract.Presenter eventListener) {
        mContext = context;
        movieList = movies;
        mEventListener = eventListener;
    }

    public void replaceData(List<Movie> movies) {
        movieList.addAll(movies);
        notifyDataSetChanged();
    }

    @Override
    protected void bindItem(BindingViewHolder<RecyclerviewItemBinding> holder, int position, List<Object> payloads) {
        Movie movie = movieList.get(position);
        holder.getBinding().setMovie(movie);
        holder.getBinding().setEventListener(mEventListener);
        Picasso.with(mContext)
                .load(BASE_IMAGE_URL + movie.getPosterPath())
                .fit()
                .centerCrop()
                .into(holder.getBinding().ivRecyclerview);

    }

    @Override
    protected int getLayoutId(int position) {
        return R.layout.recyclerview_item;
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}

