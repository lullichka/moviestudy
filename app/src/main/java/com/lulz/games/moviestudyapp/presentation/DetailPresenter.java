package com.lulz.games.moviestudyapp.presentation;

import android.util.Log;

import com.lulz.games.moviestudyapp.common.BasePresenter;
import com.lulz.games.moviestudyapp.data.DeviceStorageRemote;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

public class DetailPresenter implements DetailContract.Presenter, BasePresenter<DetailContract.View> {
    private static final String LOG_TAG = DetailPresenter.class.getSimpleName();
    private DeviceStorageRemote mDeviceStorageRemote;
    private final CompositeDisposable mCompositeDisposable;
    private DetailContract.View mView;

    @Inject
    DetailPresenter(DeviceStorageRemote deviceStorageRemote) {
        mDeviceStorageRemote = deviceStorageRemote;
        mCompositeDisposable = new CompositeDisposable();
    }


    @Override
    public void getMovie(int id) {
        mCompositeDisposable.add(mDeviceStorageRemote.getMovie(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movie -> mView.updateMovie(movie),
                        e -> Log.e(LOG_TAG, e.getMessage())));

    }

    @Override
    public void attachView(DetailContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mCompositeDisposable.dispose();
        mView = null;

    }
}
