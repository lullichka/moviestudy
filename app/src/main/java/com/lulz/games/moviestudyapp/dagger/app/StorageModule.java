package com.lulz.games.moviestudyapp.dagger.app;

import com.lulz.games.moviestudyapp.Constants;
import com.lulz.games.moviestudyapp.data.DeviceStorage;
import com.lulz.games.moviestudyapp.data.DeviceStorageRemote;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Julia Alekseeva on 10/10/18.
 */

@Module
public class StorageModule {
    @Singleton
    @Provides
    @Named(Constants.STORAGE_REMOTE)
    static DeviceStorage provideDeviceStorageRemote(DeviceStorageRemote deviceStorageRemote){
        return deviceStorageRemote;
    }
}