package com.lulz.games.moviestudyapp.dagger.app;

import com.lulz.games.moviestudyapp.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ServiceModule {
    @Singleton
    @Provides
    static ApiService provideApiService(Retrofit retrofit){
        return retrofit.create(ApiService.class);
    }
}
