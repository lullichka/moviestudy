package com.lulz.games.moviestudyapp.dagger.app;

import android.app.Service;
import android.os.Binder;

import java.lang.ref.WeakReference;

/**
 * Created by Julia Alekseeva on 10/19/18.
 */

public class LocalWordServiceBinder<S extends Service> extends Binder {
    private final WeakReference<S> mService;

    public LocalWordServiceBinder(final S service) {
        mService = new WeakReference<>(service);
    }

    public S getService() {
        return mService.get();
    }
}